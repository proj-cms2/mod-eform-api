using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using iText.Html2pdf;
using iText.Html2pdf.Css.Apply.Impl;
using iText.IO.Source;
using iText.Layout.Font;
using modEformFuncApp.Cms;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using modEformFuncApp.DBModels;
using modEformFuncApp.Dtos;
using modEformFuncApp.PostAction;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using ExecutionContext = Microsoft.Azure.WebJobs.ExecutionContext;

namespace modEformFuncApp.ApiEformModel
{
    public static class CreateOrUpdateEformModel
    {
        [FunctionName("CreateOrUpdateEformModel")]
        public static async Task<HttpResponseMessage> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "put", "post", Route = null)]
            HttpRequestMessage req, ILogger log, IDictionary<string, string> headers, ExecutionContext exeCtx)
        {
            headers.TryGetValue("Authorization", out var authHeader);

            // content type must is "application/json"
            if (!req.Content.Headers.ContentType.ToString().Contains("json"))
                return req.CreateResponse(HttpStatusCode.BadRequest,
                    "Invalid ContentType");

            string tenant = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, "tenant", true) == 0)
                .Value;


            try
            {
                var id = Guid.NewGuid().ToString();
                EformModel model;
                try
                {
                    model = await req.Content.ReadAsAsync<EformModel>();
                }
                catch (Exception e)
                {
                    return req.CreateResponse(HttpStatusCode.BadRequest, e);
                }

                var userName = Utils.GetClaimsFromToken(authHeader, "user_name");
                if (string.IsNullOrEmpty(userName))
                    return req.CreateResponse(HttpStatusCode.BadRequest,
                        "Invalid Token");

                if (string.IsNullOrEmpty(model.eformModel))
                    return req.CreateResponse(HttpStatusCode.BadRequest,
                        "Please pass a valid eform model in the request body");

                var modelEntity = new EformModel()
                {
                    id = id,
                    tenant = tenant,
                    patientId = model.patientId,
                    creationTime = DateTime.Now,
                    creatorUserId = userName,
                    lastModificationTime = null,
                    lastModifierUserId = null,
                    deletionTime = null,
                    deleterUserId = null,
                    isDeleted = null,
                    eformModel = model.eformModel,
                    eformSchema = model.eformSchema,
                    formTitle = model.formTitle,
                    formSubtitle = model.formSubtitle,
                    submitLocale = model.submitLocale
                };
                var entity = await ModelTable<EformModel>.CreateItemAsync(modelEntity);


                modelEntity.id = entity.Id;


                var schema = await SchemaTable<EformSchema>.GetRawItemAsync(modelEntity.eformSchema, tenant);
                if (schema == null)
                {
                    return req.CreateResponse(HttpStatusCode.BadRequest, "schema not found");
                }

                ThreadPool.QueueUserWorkItem(async x =>
                {
                        using (var pdfFile = new ByteArrayOutputStream())
                        {
                            var prop = new ConverterProperties();
                            prop.SetCharset("UTF-8");


                            var fontProvider = new FontProvider("Source Han Sans");
                            fontProvider.AddDirectory(exeCtx.FunctionAppDirectory + "/font");

                            prop.SetFontProvider(fontProvider);

                            HtmlConverter.ConvertToPdf(model.pdfDoc, pdfFile, prop);


                            string pdfBase64String = Convert.ToBase64String(pdfFile.ToArray());

                            await BlobStorage.UploadPdf(id, tenant, pdfBase64String);
                        }

                        var actionsJson = JsonConvert.DeserializeObject<List<dynamic>>(schema.postAction);
                        var actions = actionsJson.Select<dynamic, IPostAction>(a => ActionDeserializer.Deserialize(a));

                        var actionInvokerResponse =
                            await ActionInvoker.InvokeActionAsync(modelEntity, actions, modelEntity.id, tenant,
                                "onFormSubmit");

                        await ModelTable<EformModel>.UpdateItemAsync(entity.Id, actionInvokerResponse.Model);
                    
                });


                return req.CreateResponse(HttpStatusCode.OK,
                    new {success = true, id = entity.Id});
            }
            catch (Exception ex)
            {
                return req.CreateResponse(HttpStatusCode.InternalServerError,
                    $"The following error occurred: {ex.GetType()}: {ex.Message}");
            }
        }
    }
}