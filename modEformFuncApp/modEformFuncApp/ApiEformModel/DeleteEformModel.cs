using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using modEformFuncApp.Dtos;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Logging;

namespace modEformFuncApp.ApiEformModel
{
    public static class DeleteEformModel
    {
        [FunctionName("DeleteEformModel")]
        public static async Task<HttpResponseMessage> Run([HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)]HttpRequestMessage req, ILogger log, IDictionary<string, string> headers)
        {
            headers.TryGetValue("Authorization", out var authHeader);
            if (string.IsNullOrEmpty(authHeader))
            {
                return req.CreateResponse(HttpStatusCode.Unauthorized);
            }

            try
            {
                log.LogInformation("C# HTTP trigger function processed a request.");

                // parse query parameter
                string id = req.GetQueryNameValuePairs()
                    .FirstOrDefault(q => string.Compare(q.Key, "id", true) == 0)
                    .Value;

                string tenant = req.GetQueryNameValuePairs()
                    .FirstOrDefault(q => string.Compare(q.Key, "tenant", true) == 0)
                    .Value;

                if (string.IsNullOrEmpty(id) || string.IsNullOrEmpty(tenant)) req.CreateResponse(HttpStatusCode.BadRequest, "Please pass eform Model Id and tenant on the query string");

                Model model = await req.Content.ReadAsAsync<Model>();

                var userName = Utils.GetClaimsFromToken(authHeader, "user_name");
                if (string.IsNullOrEmpty(userName)) return req.CreateResponse(HttpStatusCode.BadRequest,
                    "Invalid Token");

                var data = await ModelTable<Model>.GetItemAsync(id, tenant);
                data.isDeleted = true;
                data.deleterUserId = userName;
                data.deletionTime = DateTime.Now;

                await ModelTable<Model>.UpdateItemAsync(data.id, data);

                return req.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return req.CreateResponse(HttpStatusCode.InternalServerError,
                    $"The following error occurred: {ex.Message}");
            }
        }
    }
}
