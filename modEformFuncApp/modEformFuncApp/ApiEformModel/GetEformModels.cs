using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.Documents.Linq;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using modEformFuncApp.Dtos;
using modEformFuncApp.DBModels;
using Microsoft.Extensions.Logging;

namespace modEformFuncApp.ApiEformModel
{
    public static class GetEformModels
    {
        [FunctionName("GetEformModels")]
        public static async Task<HttpResponseMessage> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = null)]
            HttpRequestMessage req, ILogger log)
        {
            log.LogInformation("C# HTTP trigger function processed a request.");
            try
            {
                /*
                string searchterm = req.GetQueryNameValuePairs()
                    .FirstOrDefault(q => string.Compare(q.Key, "keyword", true) == 0)
                    .Value;
                */

                string tenant = req.GetQueryNameValuePairs()
                    .FirstOrDefault(q => string.Compare(q.Key, "tenant", true) == 0)
                    .Value;

                var list = await ModelTable<Model>.GetItemsAsync(s => s.tenant.Equals(tenant));

                return req.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception ex)
            {
                return req.CreateResponse(HttpStatusCode.InternalServerError,
                    $"The following error occurred: {ex.Message}");
            }
        }
    }
}