using System;
using System.Collections.Generic;
using modEformFuncApp.DBModels;
using modEformFuncApp.Dtos;
using modEformFuncApp.PostAction;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace modEformFuncApp.ApiEformModel
{
    using Microsoft.Azure.WebJobs;
    using Microsoft.Azure.WebJobs.Extensions.Http;
    using Microsoft.Azure.WebJobs.Host;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Threading.Tasks;

    /// <summary>
    /// Defines the <see cref="GetEformModelById" />
    /// </summary>
    public static class GetEformModelById
    {
        /// <summary>
        /// The Run
        /// </summary>
        /// <param name="req">The req<see cref="HttpRequestMessage"/></param>
        /// <param name="headers" />
        /// <returns>The <see cref="Task{HttpResponseMessage}"/></returns>
        [FunctionName("GetEformModelById")]
        public static async Task<HttpResponseMessage> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)]
            HttpRequestMessage req, ILogger log, IDictionary<string, string> headers)
        {
            headers.TryGetValue("Authorization", out var authHeader);
            log.LogInformation("C# HTTP trigger function processed a request.");

            // parse query parameter
            string id = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => String.Compare(q.Key, "id", StringComparison.OrdinalIgnoreCase) == 0)
                .Value;

            string tenant = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => String.Compare(q.Key, "tenant", StringComparison.OrdinalIgnoreCase) == 0)
                .Value;

            string locale = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => String.Compare(q.Key, "locale", StringComparison.OrdinalIgnoreCase) == 0)
                .Value;

            if (id == null)
            {
                // Get request body
                dynamic data = await req.Content.ReadAsAsync<object>();
                id = data?.name;
            }

            if (tenant == null)
            {
                // Get request body
                dynamic data = await req.Content.ReadAsAsync<object>();
                tenant = data?.tenant;
            }

            if (string.IsNullOrEmpty(id) || string.IsNullOrEmpty(tenant))
            {
                return req.CreateResponse(HttpStatusCode.BadRequest,
                    "Please pass id and tenant on the query string or in the request body");
            }

            var modelTask = ModelTable<EformModel>.GetItemAsync(id, tenant);
            var model = await modelTask;
            var schemaTask = SchemaTable<EformSchema>.GetRawItemAsync(model.eformSchema, tenant);
            var schema = await schemaTask;
            if (model == null)
            {
                return req.CreateResponse(HttpStatusCode.NotFound);
            }

            if (schema == null)
            {
                return req.CreateResponse(HttpStatusCode.InternalServerError, "Record schema lost.");
            }

            var actionsJson = JsonConvert.DeserializeObject<List<dynamic>>(schema.postAction);
            var actions = actionsJson.Select<dynamic, IPostAction>(a => ActionDeserializer.Deserialize(a));

            var actionInvokerResponse =
                await ActionInvoker.InvokeActionAsync(model, actions, model.id, tenant, "onModelReturn");

            model = actionInvokerResponse.Model;
            

            schema = schema.ReplaceLanguage(locale ?? model.submitLocale);

            model.eformSchema = schema.eformSchema;


            return req.CreateResponse(HttpStatusCode.OK, model);
        }
    }
}