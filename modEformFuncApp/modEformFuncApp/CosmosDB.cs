﻿using Microsoft.Azure.Documents.Client;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace modEformFuncApp
{
    public class CosmosDB
    {
        public static DocumentClient Connect(string table)
        {
            try
            {
                return new DocumentClient(new Uri(Environment.GetEnvironmentVariable("CosmosDbEndpoint")), Environment.GetEnvironmentVariable("CosmosDbAuthKey"));

            }
            catch (Exception ex)
            {
                Console.Write(ex);
                return null;
            }
        }

    }
}
