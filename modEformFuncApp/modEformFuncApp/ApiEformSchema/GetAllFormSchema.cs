using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using modEformFuncApp.ApiEformModel;
using modEformFuncApp.DBModels;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Logging;

namespace modEformFuncApp.ApiEformSchema
{
    public static class GetAllFormSchema
    {
        [FunctionName("GetAllFormSchema")]
        public static async Task<HttpResponseMessage> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = null)]
            HttpRequestMessage req, ILogger log, IDictionary<string, string> headers)
        {
            log.LogInformation(log.GetType().FullName);
            var allForms = await SchemaTable<EformSchema>.GetItemsAsync(f => f.isDeleted != true);
            
            return req.CreateResponse(HttpStatusCode.OK, allForms);
        }
    }
}