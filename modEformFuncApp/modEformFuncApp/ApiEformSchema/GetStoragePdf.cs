using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using modEformFuncApp.DBModels;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Logging;

namespace modEformFuncApp.ApiEformSchema
{
    public static class GetStoragePdf
    {
        [FunctionName("GetStoragePdf")]
        public static async Task<HttpResponseMessage> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = null)]
            HttpRequestMessage req, ILogger log, IDictionary<string, string> headers)
        {
            string tenant = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, "tenant", StringComparison.OrdinalIgnoreCase) == 0)
                .Value;

            if (tenant == null)
            {
                return req.CreateResponse(HttpStatusCode.BadRequest, "no tenant");
            }

            var blobs = BlobStorage.GetTenantPdf(tenant);
            return req.CreateResponse(HttpStatusCode.OK, blobs);
        }
    }
}