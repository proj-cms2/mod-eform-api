using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using modEformFuncApp.DBModels;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Logging;

namespace modEformFuncApp.ApiEformSchema
{
    public static class CreateOrUpdateRootForm
    {
        [FunctionName("CreateOrUpdateRootForm")]
        public static async Task<HttpResponseMessage> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "put", "post", Route = null)]
            HttpRequestMessage req, ILogger log, IDictionary<string, string> headers)
        {
            headers.TryGetValue("Authorization", out var authHeader);
            if (string.IsNullOrEmpty(authHeader))
            {
                return req.CreateResponse(HttpStatusCode.Unauthorized);
            }


            log.LogInformation("C# HTTP trigger function processed a request.");
            try
            {
                // parse query parameter
                string id = req.GetQueryNameValuePairs()
                    .FirstOrDefault(q => string.Compare(q.Key, "id", true) == 0)
                    .Value;

                // parse query parameter
                bool updateInherit = string.Compare(req.GetQueryNameValuePairs()
                                         .FirstOrDefault(q => string.Compare(q.Key, "updateInherit", StringComparison.OrdinalIgnoreCase) == 0)
                                         .Value, "true", StringComparison.OrdinalIgnoreCase) == 0;


                // content type must is "application/json"
                if (!req.Content.Headers.ContentType.ToString().Contains("json"))
                    return req.CreateResponse(HttpStatusCode.BadRequest,
                        "Invalid ContentType");

                EformSchema schema = await req.Content.ReadAsAsync<EformSchema>();

                var creatorUserName = Utils.GetClaimsFromToken(authHeader, "user_name");
                if (string.IsNullOrEmpty(creatorUserName))
                    return req.CreateResponse(HttpStatusCode.BadRequest,
                        "Invalid Token");

                if (!string.IsNullOrEmpty(schema.eformSchema))
                {
                    if (!string.IsNullOrEmpty(id))
                    {
                        log.LogInformation("updated");
                        return await CreateOrUpdateEform.UpdateRootAsync(id, schema, creatorUserName, log, req,
                            updateInherit);
                    }
                    else
                    {
                        log.LogInformation("created");
                        return await CreateOrUpdateEform.createAsync(schema, creatorUserName, "*", req);
                    }
                }

                return req.CreateResponse(HttpStatusCode.BadRequest,
                    "Please pass a valid eformSchema in the request body");
            }
            catch (Exception ex)
            {
                return req.CreateResponse(HttpStatusCode.InternalServerError,
                    $"The following error occurred: {ex.Message}");
            }
        }
    }
}