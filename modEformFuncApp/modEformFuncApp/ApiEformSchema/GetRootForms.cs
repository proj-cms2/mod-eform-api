using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using modEformFuncApp.DBModels;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.Documents.Linq;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using modEformFuncApp.Dtos;
using Microsoft.Extensions.Logging;

namespace modEformFuncApp.ApiEformSchema
{
    public static class GetRootforms
    {
        [FunctionName("GetRootforms")]
        public static async Task<HttpResponseMessage> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = null)]
            HttpRequestMessage req, ILogger log)
        {
            log.LogInformation("C# HTTP trigger function processed a request.");
            try
            {
                var list = await SchemaTable<EformSchema>.GetItemsAsync(s =>
                    s.tenant == "*" && s.isDeleted != true);

                return req.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception ex)
            {
                log.LogInformation(ex.Message);
                return req.CreateResponse(HttpStatusCode.InternalServerError,
                    $"The following error occurred: {ex.Message}");
            }
        }
    }
}