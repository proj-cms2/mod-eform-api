using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using JsonDiffPatchDotNet;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace modEformFuncApp.ApiEformSchema
{
    using Microsoft.Azure.WebJobs;
    using Microsoft.Azure.WebJobs.Extensions.Http;
    using Microsoft.Azure.WebJobs.Host;
    using modEformFuncApp.Dtos;
    using modEformFuncApp.DBModels;
    using System;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Threading.Tasks;

    /// <summary>
    /// Defines the <see cref="CreateOrUpdateEform" />
    /// </summary>
    public static class CreateOrUpdateEform
    {
        /// <summary>
        /// The Run
        /// </summary>
        /// <param name="req">The req<see cref="HttpRequestMessage"/></param>
        /// <param name="outDoc">The outDoc<see cref="ICollector{EformSchema}"/></param>
        /// <returns>The <see cref="Task{HttpResponseMessage}"/></returns>
        [FunctionName("CreateOrUpdateEform")]
        public static async Task<HttpResponseMessage> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "put", "post", Route = null)]
            HttpRequestMessage req, ILogger log, IDictionary<string, string> headers)
        {
            headers.TryGetValue("Authorization", out var authHeader);
            if (string.IsNullOrEmpty(authHeader))
            {
                return req.CreateResponse(HttpStatusCode.Unauthorized);
            }


            log.LogInformation("C# HTTP trigger function processed a request.");
            try
            {
                // parse query parameter
                string id = req.GetQueryNameValuePairs()
                    .FirstOrDefault(q => string.Compare(q.Key, "id", true) == 0)
                    .Value;

                // parse query parameter
                string tenant = req.GetQueryNameValuePairs()
                    .FirstOrDefault(q => string.Compare(q.Key, "tenant", true) == 0)
                    .Value;

                if (tenant == "*")
                {
                    return req.CreateResponse(HttpStatusCode.Unauthorized, "Unauthorize to edit root form");
                }

                // content type must is "application/json"
                if (!req.Content.Headers.ContentType.ToString().Contains("json"))
                    return req.CreateResponse(HttpStatusCode.BadRequest,
                        "Invalid ContentType");

                EformSchema schema = await req.Content.ReadAsAsync<EformSchema>();

                var creatorUserName = Utils.GetClaimsFromToken(authHeader, "user_name");
                if (string.IsNullOrEmpty(creatorUserName))
                    return req.CreateResponse(HttpStatusCode.BadRequest,
                        "Invalid Token");

                if (!string.IsNullOrEmpty(schema.eformSchema))
                {
                    if (!string.IsNullOrEmpty(id))
                    {
                        log.LogInformation("updated");
                        return await updateAsync(id, schema, creatorUserName, tenant, log, req);
                    }
                    else
                    {
                        log.LogInformation("created");
                        return await createAsync(schema, creatorUserName, tenant, req);
                    }
                }

                return req.CreateResponse(HttpStatusCode.BadRequest,
                    "Please pass a valid eformSchema in the request body");
            }
            catch (Exception ex)
            {
                return req.CreateResponse(HttpStatusCode.InternalServerError,
                    $"The following error occurred: {ex.Message}");
            }
        }

        /// <summary>
        /// The update
        /// </summary>
        /// <param name="id">The id<see cref="string"/></param>
        /// <param name="schema">The schema<see cref="Schema"/></param>
        /// <param name="creatorUserName">The creatorUserName<see cref="string"/></param>
        public static async Task<HttpResponseMessage> updateAsync(string id, EformSchema schema, string userName,
            string tenant,
            ILogger log, HttpRequestMessage req)
        {
            var data = await SchemaTable<EformSchema>.GetItemAsync(id, tenant);

            //var oldData = await SchemaTable<Schema>.GetItemsAsync(s => s.id == id);
            //var data = oldData.FirstOrDefault();
            if (data == null)
            {
                return req.CreateResponse(HttpStatusCode.NotFound, "Schema not found");
            }


            if (data.tenant == tenant)
            {
                data.isDeleted = true;
                /*
                data.deletionTime = DateTime.Now;
                data.deleterUserId = userName;
                */

                data.lastModificationTime = DateTime.Now;
                data.lastModifierUserId = userName;
                await SchemaTable<EformSchema>.UpdateItemAsync(data.id, data);
            }


            await SchemaTable<EformSchema>.CreateItemAsync(new EformSchema()
            {
                rootFormId = id,
                creationTime = data.creationTime,
                creatorUserId = data.creatorUserId,
                lastModificationTime = DateTime.Now,
                lastModifierUserId = userName,
                deletionTime = null,
                deleterUserId = null,
                isDeleted = null,
                eformName = schema.eformName,
                eformSchema = schema.eformSchema,
                eformLocale = schema.eformLocale,
                defaultLanguage = schema.defaultLanguage,
                eformDefaultModel = null,
                zh_HK_eformSchema = null,
                zh_HK_eformDefaultModel = null,
                version = data.version + 1,
                tenant = tenant,
                postAction = schema.postAction,
                rootFormVersion = data.rootFormVersion,
                isPrivate = data.isPrivate ?? schema.isPrivate ?? false
            });
            return req.CreateResponse(HttpStatusCode.OK);
        }


        public static async Task UpdateSchemaAsync(string id, string tenant, EformSchema newSchema,
            EformSchema oldSchema, string userName)
        {
            if (oldSchema.tenant == tenant || tenant == "*")
            {
                oldSchema.isDeleted = true;
                /*
                oldSchema.deletionTime = DateTime.Now;
                oldSchema.deleterUserId = userName;
                */
                oldSchema.lastModificationTime = DateTime.Now;
                oldSchema.lastModifierUserId = userName;
                await SchemaTable<EformSchema>.UpdateItemAsync(oldSchema.id, oldSchema);
            }


            await SchemaTable<EformSchema>.CreateItemAsync(new EformSchema()
            {
                rootFormId = oldSchema.rootFormId,
                creationTime = oldSchema.creationTime,
                creatorUserId = oldSchema.creatorUserId,
                lastModificationTime = DateTime.Now,
                lastModifierUserId = userName,
                deletionTime = null,
                deleterUserId = null,
                isDeleted = null,
                eformName = newSchema.eformName,
                eformSchema = newSchema.eformSchema,
                eformLocale = newSchema.eformLocale,
                defaultLanguage = newSchema.defaultLanguage,
                eformDefaultModel = null,
                zh_HK_eformSchema = null,
                zh_HK_eformDefaultModel = null,
                version = oldSchema.version + 1,
                tenant = tenant,
                postAction = newSchema.postAction,
                rootFormVersion = newSchema.rootFormVersion,
                isPrivate = newSchema.isPrivate ?? oldSchema.isPrivate ?? false
            });
        }

        public static async Task<HttpResponseMessage> UpdateRootAsync(string id, EformSchema schema, string userName,
            ILogger log, HttpRequestMessage req, bool patchInherit)
        {
            var data = await SchemaTable<EformSchema>.GetItemAsync(id, "*");

            if (data == null)
            {
                return req.CreateResponse(HttpStatusCode.NotFound, "Schema not found");
            }

            if (data.tenant != "*")
            {
                return req.CreateResponse(HttpStatusCode.BadRequest, "Not a root form");
            }


            if (patchInherit)
            {
                var patch = new JsonDiffPatch();


                var schemaDiff = patch.Diff(JToken.Parse(data.eformSchema) ?? JToken.Parse("{}"),
                    JToken.Parse(schema.eformSchema) ?? JToken.Parse("{}"));


                var localeDiff = patch.Diff(JToken.Parse(data.eformLocale) ?? JToken.Parse("{}"),
                    JToken.Parse(schema.eformLocale) ?? JToken.Parse("{}"));


                var postActionDiff = patch.Diff(JToken.Parse(data.postAction) ?? JToken.Parse("[]"),
                    JToken.Parse(schema.postAction) ?? JToken.Parse("[]"));


                var inheritTable = await SchemaTable<EformSchema>.GetItemsAsync(s =>
                    s.rootFormId == data.rootFormId && s.tenant != "*" && s.isDeleted != true);

                foreach (var schemaInherit in inheritTable)
                {
                    log.LogInformation(schemaInherit.id);
                    var newSchema = JToken.Parse(schemaInherit.eformSchema ?? "{}");
                    if (schemaDiff != null)
                    {
                        newSchema = patch.Patch(newSchema, schemaDiff);
                    }


                    var newLocale = JToken.Parse(schemaInherit.eformLocale ?? "{}");
                    if (localeDiff != null)
                    {
                        newLocale = patch.Patch(newLocale, localeDiff);
                    }

                    var newPostAction = JToken.Parse(schemaInherit.postAction ?? "[]");
                    if (postActionDiff != null)
                    {
                        newPostAction = patch.Patch(newPostAction, postActionDiff);
                    }

                    var updatedSchema = new EformSchema()
                    {
                        eformName = schemaInherit.eformName,
                        defaultLanguage = schemaInherit.defaultLanguage,
                        eformSchema = newSchema.ToString(),
                        eformLocale = newLocale.ToString(),
                        postAction = newPostAction.ToString(),
                        version = schemaInherit.version + 1,
                        creationTime = schemaInherit.creationTime,
                        creatorUserId = schemaInherit.creatorUserId,
                        isPrivate = false
                    };

                    await UpdateSchemaAsync(id, schemaInherit.tenant, updatedSchema, schemaInherit, userName);
                    log.LogInformation($"Updated inherit schema: id: ${id} tenant: ${schemaInherit.tenant}");
                }
            }

            data.isDeleted = true;
            /*
            data.deletionTime = DateTime.Now;
            data.deleterUserId = userName;
            */

            data.lastModificationTime = DateTime.Now;
            data.lastModifierUserId = userName;
            await SchemaTable<EformSchema>.UpdateItemAsync(data.id, data);


            await SchemaTable<EformSchema>.CreateItemAsync(new EformSchema()
            {
                rootFormId = id,
                creationTime = data.creationTime,
                creatorUserId = data.creatorUserId,
                lastModificationTime = DateTime.Now,
                lastModifierUserId = userName,
                deletionTime = null,
                deleterUserId = null,
                isDeleted = null,
                eformName = schema.eformName,
                eformSchema = schema.eformSchema,
                eformLocale = schema.eformLocale,
                defaultLanguage = schema.defaultLanguage,
                eformDefaultModel = null,
                zh_HK_eformSchema = null,
                zh_HK_eformDefaultModel = null,
                version = data.version + 1,
                tenant = "*",
                postAction = schema.postAction,
                rootRemark = schema.rootRemark
            });
            return req.CreateResponse(HttpStatusCode.OK);
        }


        /// <summary>
        /// The create
        /// </summary>
        /// <param name="id">The id<see cref="string"/></param>
        /// <param name="schema">The schema<see cref="Schema"/></param>
        /// <param name="creatorUserName">The creatorUserName<see cref="string"/></param>
        public static async Task<HttpResponseMessage> createAsync(EformSchema schema, string creatorUserName,
            string tenant, HttpRequestMessage req)
        {
            var id = Guid.NewGuid().ToString();

            await SchemaTable<EformSchema>.CreateItemAsync(new EformSchema()
            {
                id = id,
                rootFormId = id,
                creationTime = DateTime.Now,
                creatorUserId = creatorUserName,
                lastModificationTime = DateTime.Now,
                lastModifierUserId = creatorUserName,
                deletionTime = null,
                deleterUserId = null,
                isDeleted = null,
                eformName = schema.eformName,
                eformSchema = schema.eformSchema,
                eformLocale = schema.eformLocale,
                defaultLanguage = schema.defaultLanguage,
                eformDefaultModel = null,
                zh_HK_eformSchema = null,
                zh_HK_eformDefaultModel = null,
                version = 1,
                tenant = tenant,
                postAction = schema.postAction,
                rootRemark = schema.rootRemark,
                isPrivate = schema.isPrivate ?? false
            });
            return req.CreateResponse(HttpStatusCode.OK);
        }
    }
}