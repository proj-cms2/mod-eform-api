using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using modEformFuncApp.DBModels;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Logging;

namespace modEformFuncApp.ApiEformSchema
{
    public static class RevertEform
    {
        [FunctionName("RevertEform")]
        public static async Task<HttpResponseMessage> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post")]
            HttpRequestMessage req, ILogger log, IDictionary<string, string> headers)
        {
            headers.TryGetValue("Authorization", out var authHeader);
            if (string.IsNullOrEmpty(authHeader))
            {
                return req.CreateResponse(HttpStatusCode.Unauthorized);
            }

            var creatorUserName = Utils.GetClaimsFromToken(authHeader, "user_name");
            if (string.IsNullOrEmpty(creatorUserName))
                return req.CreateResponse(HttpStatusCode.BadRequest,
                    "Invalid Token");


            dynamic data = await req.Content.ReadAsAsync<object>();


            string rootFormId = data?.rootFormId;
            string tenant = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, "tenant", true) == 0)
                .Value;
            string version = data?.version;

            var sourceSchema = (await SchemaTable<EformSchema>.GetItemsAsync(e =>
                e.rootFormId == rootFormId && e.tenant == tenant && e.version == int.Parse(version))).FirstOrDefault();

            var currentSchema = await SchemaTable<EformSchema>.GetItemAsync(rootFormId, tenant);
            if (currentSchema == null || sourceSchema == null)
            {
                return req.CreateResponse(HttpStatusCode.BadRequest, "Can't find form");
            }

            if (currentSchema.tenant != "*")
            {
                currentSchema.isDeleted = true;
                currentSchema.lastModifierUserId = creatorUserName;
                currentSchema.lastModificationTime = DateTime.Now;
                await SchemaTable<EformSchema>.UpdateItemAsync(currentSchema.id, currentSchema);
            }


            sourceSchema.id = null;
            sourceSchema.tenant = tenant;
            sourceSchema.version = currentSchema.version + 1;
            sourceSchema.lastModificationTime = DateTime.Now;
            sourceSchema.lastModifierUserId = creatorUserName;
            sourceSchema.isDeleted = false;
            sourceSchema.deleterUserId = null;
            sourceSchema.deletionTime = null;
            sourceSchema.tenantFormSourceVersion = sourceSchema.version;

            await SchemaTable<EformSchema>.CreateItemAsync(sourceSchema);
            return req.CreateResponse(HttpStatusCode.OK, "OK");
        }
    }
}