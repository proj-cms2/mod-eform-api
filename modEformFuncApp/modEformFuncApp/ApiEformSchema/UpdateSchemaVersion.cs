using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using JsonDiffPatchDotNet;
using modEformFuncApp.DBModels;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;

namespace modEformFuncApp.ApiEformSchema
{
    public static class UpdateSchemaVersion
    {
        [FunctionName("UpdateSchemaVersion")]
        public static async Task<HttpResponseMessage> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post")]
            HttpRequestMessage req, ILogger log, IDictionary<string, string> headers)
        {
            headers.TryGetValue("Authorization", out var authHeader);
            if (string.IsNullOrEmpty(authHeader))
            {
                return req.CreateResponse(HttpStatusCode.Unauthorized);
            }

            var creatorUserName = Utils.GetClaimsFromToken(authHeader, "user_name");


            dynamic data = await req.Content.ReadAsAsync<object>();


            string rootFormId = data?.rootFormId;
            string tenant = data?.tenant;
            int version = data?.version;


            var currentVersion = await SchemaTable<EformSchema>.GetItemAsync(rootFormId, tenant);


            var rootForm = (await SchemaTable<EformSchema>.GetItemsAsync(e =>
                e.rootFormId == rootFormId && e.tenant == "*" && e.version == version)).FirstOrDefault();

            if (currentVersion == null || rootForm == null)
            {
                return req.CreateResponse(HttpStatusCode.NotFound, "Schema not found");
            }

            if (currentVersion.rootFormVersion == null)
            {
                currentVersion.rootFormVersion = rootForm.version;
                /* Don't Override
                var revertedForm = new EformSchema()
                {
                    eformName = rootForm.eformName,
                    formLocale = rootForm.formLocale,
                    eformSchema = rootForm.eformSchema,
                    postAction = rootForm.postAction,
                    tenant = tenant,
                    version = currentVersion.version + 1,
                    creationTime = currentVersion.creationTime,
                    creatorUserId = currentVersion.creatorUserId,
                    lastModificationTime = DateTime.Now,
                    lastModifierUserId = creatorUserName,
                    rootFormVersion = rootForm.version,
                    rootFormId = rootForm.rootFormId,
                    defaultLanguage = rootForm.defaultLanguage,
                    isDeleted = false
                };


                if (currentVersion.tenant != "*")
                {
                    currentVersion.isDeleted = true;
                    currentVersion.lastModifierUserId = creatorUserName;
                    currentVersion.lastModificationTime = DateTime.Now;
                    await SchemaTable<EformSchema>.UpdateItemAsync(currentVersion.id, currentVersion);
                }

                await SchemaTable<EformSchema>.CreateItemAsync(revertedForm);
                return req.CreateResponse(HttpStatusCode.OK, "OK, Overrided");
                */
            }
            else
            {
                if (version == currentVersion.rootFormVersion)
                {
                    return req.CreateResponse(HttpStatusCode.OK, "Unchanged Root form version, not update required.");
                }

                rootForm = currentVersion;
            }

            var rootFormOriginal
                = (await SchemaTable<EformSchema>.GetItemsAsync(e =>
                    e.rootFormId == rootFormId && e.tenant == "*" && e.version == currentVersion.rootFormVersion))
                .FirstOrDefault();
            if (rootFormOriginal == null)
            {
                return req.CreateResponse(HttpStatusCode.NotFound, "original root form not found");
            }


            var patch = new JsonDiffPatch();


            var schemaDiff = patch.Diff(JToken.Parse(rootFormOriginal.eformSchema) ?? JToken.Parse("{}"),
                JToken.Parse(rootForm.eformSchema) ?? JToken.Parse("{}"));


            var localeDiff = patch.Diff(JToken.Parse(rootFormOriginal.eformLocale) ?? JToken.Parse("{}"),
                JToken.Parse(rootForm.eformLocale) ?? JToken.Parse("{}"));


            var postActionDiff = patch.Diff(JToken.Parse(rootFormOriginal.postAction) ?? JToken.Parse("[]"),
                JToken.Parse(rootForm.postAction) ?? JToken.Parse("[]"));


            var newSchema = JToken.Parse(currentVersion.eformSchema ?? "{}");
            if (schemaDiff != null)
            {
                newSchema = patch.Patch(newSchema, schemaDiff);
            }


            var newLocale = JToken.Parse(currentVersion.eformLocale ?? "{}");
            if (localeDiff != null)
            {
                newLocale = patch.Patch(newLocale, localeDiff);
            }

            var newPostAction = JToken.Parse(currentVersion.postAction ?? "[]");
            if (postActionDiff != null)
            {
                newPostAction = patch.Patch(newPostAction, postActionDiff);
            }

            var updatedSchema = new EformSchema()
            {
                eformName = currentVersion.eformName,
                defaultLanguage = currentVersion.defaultLanguage,
                eformSchema = newSchema.ToString(),
                eformLocale = newLocale.ToString(),
                postAction = newPostAction.ToString(),
                version = currentVersion.version + 1,
                creationTime = currentVersion.creationTime,
                creatorUserId = currentVersion.creatorUserId,
                tenant = tenant,
                lastModificationTime = DateTime.Now,
                lastModifierUserId = creatorUserName,
                rootFormVersion = rootForm.version,
                rootFormId = currentVersion.rootFormId,
                isDeleted = false
            };

            await CreateOrUpdateEform.UpdateSchemaAsync(currentVersion.id, currentVersion.tenant, updatedSchema,
                currentVersion, creatorUserName);
            return req.CreateResponse(HttpStatusCode.OK, "OK");
        }
    }
}