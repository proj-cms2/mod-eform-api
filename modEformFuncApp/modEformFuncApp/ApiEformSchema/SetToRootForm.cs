using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using modEformFuncApp.DBModels;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Logging;

namespace modEformFuncApp.ApiEformSchema
{
    public static class SetToRootForm
    {
        [FunctionName("SetToRootForm")]
        public static async Task<HttpResponseMessage> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = null)]
            HttpRequestMessage req, ILogger log, IDictionary<string, string> headers)
        {
            log.LogInformation("C# HTTP trigger function processed a request.");

            headers.TryGetValue("Authorization", out var authHeader);

            var creatorUserName = Utils.GetClaimsFromToken(authHeader, "user_name");
            if (string.IsNullOrEmpty(creatorUserName))
                return req.CreateResponse(HttpStatusCode.BadRequest,
                    "Invalid Token");

            // parse query parameter
            string tenant = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, "tenant", true) == 0)
                .Value;

            // Get request body
            dynamic data = await req.Content.ReadAsAsync<object>();
            string rawId = data?.id;

            EformSchema form = await SchemaTable<EformSchema>.GetRawItemAsync(rawId, tenant);
            if (form == null)
            {
                return req.CreateResponse(HttpStatusCode.NotFound, "Raw form not found");
            }


            EformSchema rootForm = (await SchemaTable<EformSchema>.GetItemsAsync(r =>
                r.tenant == "*" && r.rootFormId == form.rootFormId && r.isDeleted != true)).FirstOrDefault();
            if (rootForm != null)
            {
                form.version = rootForm.version;
            }

            await CreateOrUpdateEform.UpdateSchemaAsync(form.rootFormId, "*", form, rootForm ?? form,
                creatorUserName);
            return req.CreateResponse(HttpStatusCode.OK);
        }
    }
}