using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using modEformFuncApp.DBModels;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Logging;

namespace modEformFuncApp.ApiEformSchema
{
    public static class CopyToTenant
    {
        [FunctionName("CopyToTenant")]
        public static async Task<HttpResponseMessage> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)]
            HttpRequestMessage req, ILogger log, IDictionary<string, string> headers)
        {
            log.LogInformation("C# HTTP trigger function processed a request.");

            headers.TryGetValue("Authorization", out var authHeader);

            var creatorUserName = Utils.GetClaimsFromToken(authHeader, "user_name");
            if (string.IsNullOrEmpty(creatorUserName))
                return req.CreateResponse(HttpStatusCode.BadRequest,
                    "Invalid Token");

            // parse query parameter
            string fromTenant = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, "tenant", true) == 0)
                .Value;

            // Get request body
            dynamic data = await req.Content.ReadAsAsync<object>();
            string formId = data?.id;
            string toTenant = data?.tenant;

            if (string.IsNullOrWhiteSpace(toTenant) || toTenant == "*")
            {
                return req.CreateResponse(HttpStatusCode.BadRequest);
            }

            EformSchema originalForm = await SchemaTable<EformSchema>.GetRawItemAsync(formId, fromTenant);
            if (originalForm == null)
            {
                return req.CreateResponse(HttpStatusCode.NotFound);
            }

            EformSchema tenantForm = await SchemaTable<EformSchema>.GetItemAsync(originalForm.rootFormId, toTenant);


            await CreateOrUpdateEform.UpdateSchemaAsync(originalForm.rootFormId, toTenant, originalForm,
                tenantForm ?? originalForm,
                creatorUserName);

            return req.CreateResponse(HttpStatusCode.OK);
        }
    }
}