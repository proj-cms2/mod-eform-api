using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using modEformFuncApp.DBModels;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.Documents.Linq;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using modEformFuncApp.Dtos;
using Microsoft.Extensions.Logging;

namespace modEformFuncApp.ApiEformSchema
{
    public static class GetEforms
    {
        [FunctionName("GetEforms")]
        public static async Task<HttpResponseMessage> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = null)]
            HttpRequestMessage req, ILogger log)
        {
            log.LogInformation("C# HTTP trigger function processed a request.");
            try
            {
                string tenant = req.GetQueryNameValuePairs()
                    .FirstOrDefault(q => string.Compare(q.Key, "tenant", true) == 0)
                    .Value;

                if (tenant == "*")
                {
                    return req.CreateResponse(HttpStatusCode.Unauthorized, "Not permission to get root form");
                }

                var list = await SchemaTable<EformSchema>.GetItemsAsync(s =>
                    (s.tenant == tenant || s.tenant == "*") && s.isDeleted != true);
                var formList = list.Select(s => s.rootFormId).Distinct();
                var distictList = formList.Select(s =>
                {
                    var spec = list.FirstOrDefault(s2 => s2.rootFormId == s && s2.tenant == tenant);
                    if (spec != null) return spec;
                    return list.FirstOrDefault(s2 => s2.rootFormId == s && s2.tenant == "*");
                });
                return req.CreateResponse(HttpStatusCode.OK, distictList);
            }
            catch (Exception ex)
            {
                log.LogInformation(ex.Message);
                return req.CreateResponse(HttpStatusCode.InternalServerError,
                    $"The following error occurred: {ex.Message}");
            }
        }
    }
}