using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using modEformFuncApp.DBModels;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using modEformFuncApp.Dtos;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace modEformFuncApp.ApiEformSchema
{
    public static class GetEformSchemaById
    {
        [FunctionName("GetEformSchemaById")]
        public static async Task<HttpResponseMessage> RunAsync(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = null)]
            HttpRequestMessage req, ILogger log)
        {
            try
            {
                log.LogInformation("C# HTTP trigger function processed a request.");

                // parse query parameter
                string id = req.GetQueryNameValuePairs()
                    .FirstOrDefault(q => string.Compare(q.Key, "id", true) == 0)
                    .Value;

                string tenant = req.GetQueryNameValuePairs()
                    .FirstOrDefault(q => string.Compare(q.Key, "tenant", true) == 0)
                    .Value;

                string locale = req.GetQueryNameValuePairs()
                    .FirstOrDefault(q => string.Compare(q.Key, "locale", true) == 0)
                    .Value;

                if (id == null)
                {
                    // Get request body
                    dynamic data = await req.Content.ReadAsAsync<object>();
                    id = data?.name;
                }

                if (tenant == null)
                {
                    // Get request body
                    dynamic data = await req.Content.ReadAsAsync<object>();
                    tenant = data?.tenant;
                }

                if (string.IsNullOrEmpty(id) || string.IsNullOrEmpty(tenant))
                {
                    return req.CreateResponse(HttpStatusCode.BadRequest,
                        "Please pass id and tenant on the query string or in the request body");
                }

                //var schemaItem = await SchemaTable<Schema>.GetItemAsync(id);
                var schemaItem = await SchemaTable<EformSchema>.GetItemAsync(id, tenant);

                if (schemaItem == null)
                {
                    return req.CreateResponse(HttpStatusCode.NotFound);
                }

                Utils.log = log;
                return req.CreateResponse(HttpStatusCode.OK, schemaItem.ReplaceLanguage(locale));
            }
            catch (Exception ex)
            {
                return req.CreateResponse(HttpStatusCode.InternalServerError,
                    $"The following error occurred: {ex.Message}");
            }
        }
    }
}