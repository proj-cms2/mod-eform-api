using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using AzureFunctions.Autofac;
using modEformFuncApp.DBModels;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Logging;

namespace modEformFuncApp.ApiEformSchema
{
    [DependencyInjectionConfig(typeof(DependencyInjectionConfig))]
    public static class GetAllTenant
    {
        [FunctionName("GetAllTenant")]
        public static async Task<HttpResponseMessage> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = null)]
            HttpRequestMessage req, [Inject] ILogger log, IDictionary<string, string> headers)
        {
            var tenant = await SchemaTable<EformSchema>.GetTenant();

            return req.CreateResponse(HttpStatusCode.OK, tenant);
        }
    }
}