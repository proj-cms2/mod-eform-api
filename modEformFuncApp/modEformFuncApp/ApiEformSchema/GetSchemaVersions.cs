using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using modEformFuncApp.DBModels;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Logging;

namespace modEformFuncApp.ApiEformSchema
{
    public static class GetSchemaVersions
    {
        [FunctionName("GetSchemaVersions")]
        public static async Task<HttpResponseMessage> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = null)]
            HttpRequestMessage req, ILogger log, IDictionary<string, string> headers)
        {
            headers.TryGetValue("Authorization", out var authHeader);
            if (string.IsNullOrEmpty(authHeader))
            {
                return req.CreateResponse(HttpStatusCode.Unauthorized);
            }

            // parse query parameter
            string tenant = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, "tenant", StringComparison.OrdinalIgnoreCase) == 0)
                .Value;

            string rootFormId = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, "rootFormId", StringComparison.OrdinalIgnoreCase) == 0)
                .Value;

            var schemas = await SchemaTable<EformSchema>.GetItemsAsync(s =>
                s.rootFormId == rootFormId && (s.tenant == tenant || s.tenant == "*"));

            return req.CreateResponse(HttpStatusCode.OK, schemas);
        }
    }
}