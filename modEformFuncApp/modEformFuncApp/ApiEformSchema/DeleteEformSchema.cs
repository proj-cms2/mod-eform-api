using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using modEformFuncApp.DBModels;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using modEformFuncApp.Dtos;
using Microsoft.Extensions.Logging;

namespace modEformFuncApp.ApiEformSchema
{
    public static class DeleteEformSchema
    {
        [FunctionName("DeleteEformSchema")]
        public static async System.Threading.Tasks.Task<HttpResponseMessage> RunAsync(
            [HttpTrigger(AuthorizationLevel.Anonymous, "put", "post", Route = null)]
            HttpRequestMessage req, ILogger log, IDictionary<string, string> headers)
        {
            headers.TryGetValue("Authorization", out var authHeader);
            if (string.IsNullOrEmpty(authHeader))
            {
                return req.CreateResponse(HttpStatusCode.Unauthorized);
            }

            try
            {
                log.LogInformation("C# HTTP trigger function processed a request.");

                // parse query parameter
                string id = req.GetQueryNameValuePairs()
                    .FirstOrDefault(q => string.Compare(q.Key, "id", true) == 0)
                    .Value;

                string tenant = req.GetQueryNameValuePairs()
                    .FirstOrDefault(q => string.Compare(q.Key, "tenant", true) == 0)
                    .Value;

                if (string.IsNullOrEmpty(id) || string.IsNullOrEmpty(tenant))
                    req.CreateResponse(HttpStatusCode.BadRequest,
                        "Please pass eform Schema Id and tenant on the query string");

                var schema = await req.Content.ReadAsAsync<EformSchema>();

                var userName = Utils.GetClaimsFromToken(authHeader, "user_name");
                if (string.IsNullOrEmpty(userName))
                    return req.CreateResponse(HttpStatusCode.BadRequest,
                        "Invalid Token");

                var data = await SchemaTable<EformSchema>.GetItemAsync(id, tenant);

                if (data.tenant == "*") return req.CreateResponse(HttpStatusCode.Forbidden);
                //var datas = await SchemaTable<Schema>.GetItemsAsync(s => s.id == id);
                //var data = datas.FirstOrDefault();
                data.isDeleted = true;
                /*
                data.deleterUserId = userName;
                data.deletionTime = DateTime.Now;
                */

                await SchemaTable<EformSchema>.UpdateItemAsync(data.id, data);

                return req.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return req.CreateResponse(HttpStatusCode.InternalServerError,
                    $"The following error occurred: {ex.Message}");
            }
        }
    }
}