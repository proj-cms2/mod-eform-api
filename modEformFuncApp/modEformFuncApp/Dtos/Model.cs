﻿using System;

namespace modEformFuncApp.Dtos
{
    public class Model : CommonDto
    {
        public string id { get; set; }
        public string tenant { get; set; }
        public long patientId { get; set; } = 0;
        public bool? patientCreated { get; set; }
        public DateTime creationTime { get; set; }
        public string creatorUserId { get; set; }
        public DateTime? lastModificationTime { get; set; }
        public string lastModifierUserId { get; set; }
        public DateTime? deletionTime { get; set; }
        public string deleterUserId { get; set; }
        public bool? isDeleted { get; set; }
        public string eformModel { get; set; }
        public string eformSchema { get; set; }
        public string formTitle { get; set; }
        public string formSubtitle { get; set; }
        public string pdfDoc { get; set; }
        public string submitLocale { get; set; }

    }
}