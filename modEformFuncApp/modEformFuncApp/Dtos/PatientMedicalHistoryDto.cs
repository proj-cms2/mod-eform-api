﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace modEformFuncApp.Dtos
{
    public class PatientMedicalHistoryDto
    {
        public string historyId { set; get; }
        public string medicalHistoryName { set; get; }
    }
}
