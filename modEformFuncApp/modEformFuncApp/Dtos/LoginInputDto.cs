﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace modEformFuncApp.Dtos
{
    public class LoginInputDto
    {
        public string tenant { get; set; }
        public string user { get; set; }
        public string pass { get; set; }
    }
}
