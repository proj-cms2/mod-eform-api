﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace modEformFuncApp.Dtos
{
    public class UploadAttachmentDto:CommonDto
    {
        public int patientId;

        public MultipartFileData file;
    }
}
