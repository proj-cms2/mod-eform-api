﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace modEformFuncApp.Dtos
{
    public class LoginOutputDto
    {
        public string access_token { set; get; }
        public string token_type { set; get; }
        public string expires_in { set; get; }
    }
}
