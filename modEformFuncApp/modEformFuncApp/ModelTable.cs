﻿namespace modEformFuncApp
{
    using Microsoft.Azure.Documents;
    using Microsoft.Azure.Documents.Client;
    using Microsoft.Azure.Documents.Linq;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;

    /// <summary>
    /// Defines the <see cref="ModelTable{T}" />
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ModelTable<T> where T : class
    {
        /// <summary>
        /// Defines the client
        /// </summary>
        private static DocumentClient client = CosmosDB.Connect(Environment.GetEnvironmentVariable("CosmosDbSchemaCollection"));

        /// <summary>
        /// Defines the DatabaseId
        /// </summary>
        private static string DatabaseId = Environment.GetEnvironmentVariable("CosmosDbDatabase");

        /// <summary>
        /// Defines the CollectionId
        /// </summary>
        private static string CollectionId = Environment.GetEnvironmentVariable("CosmosDbModelCollection");

        /// <summary>
        /// The GetItemAsync
        /// </summary>
        /// <param name="id">The id<see cref="string"/></param>
        /// /// <param name="tenant">The tenant<see cref="string"/></param>
        /// <returns>The <see cref="Task{T}"/></returns>
        public static async Task<T> GetItemAsync(string id,string tenant)
        {
            try
            {
                Document document = await client.ReadDocumentAsync(UriFactory.CreateDocumentUri(DatabaseId, CollectionId, id), new RequestOptions { PartitionKey = new PartitionKey(tenant) });
                return (T)(dynamic)document;
            }
            catch (DocumentClientException e)
            {
                if (e.StatusCode == System.Net.HttpStatusCode.NotFound)
                {
                    return null;
                }
                else
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// The GetItemsAsync
        /// </summary>
        /// <param name="predicate">The predicate<see cref="Expression{Func{T, bool}}"/></param>
        /// <returns>The <see cref="Task{IEnumerable{T}}"/></returns>
        public static async Task<IEnumerable<T>> GetItemsAsync(Expression<Func<T, bool>> predicate)
        {
            IDocumentQuery<T> query = client.CreateDocumentQuery<T>(
                UriFactory.CreateDocumentCollectionUri(DatabaseId, CollectionId),
                new FeedOptions { MaxItemCount = -1, EnableCrossPartitionQuery = true })
                .Where(predicate)
                .AsDocumentQuery();

            List<T> results = new List<T>();
            while (query.HasMoreResults)
            {
                results.AddRange(await query.ExecuteNextAsync<T>());
            }

            return results;
        }

        /// <summary>
        /// The CreateItemAsync
        /// </summary>
        /// <param name="item">The item<see cref="T"/></param>
        /// <returns>The <see cref="Task{Document}"/></returns>
        public static async Task<Document> CreateItemAsync(T item)
        {
            return await client.CreateDocumentAsync(UriFactory.CreateDocumentCollectionUri(DatabaseId, CollectionId), item);
        }

        /// <summary>
        /// The UpdateItemAsync
        /// </summary>
        /// <param name="id">The id<see cref="string"/></param>
        /// <param name="item">The item<see cref="T"/></param>
        /// <returns>The <see cref="Task{Document}"/></returns>
        public static async Task<Document> UpdateItemAsync(string id, T item)
        {
            return await client.ReplaceDocumentAsync(UriFactory.CreateDocumentUri(DatabaseId, CollectionId, id), item);
        }

        /// <summary>
        /// The DeleteItemAsync
        /// </summary>
        /// <param name="id">The id<see cref="string"/></param>
        /// <returns>The <see cref="Task"/></returns>
        public static async Task DeleteItemAsync(string id)
        {
            await client.DeleteDocumentAsync(UriFactory.CreateDocumentUri(DatabaseId, CollectionId, id));
        }
    }
}
