﻿using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using AzureFunctions.Autofac.Configuration;
using log4net;
using log4net.Appender;
using log4net.Core;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using ILogger = Microsoft.Extensions.Logging.ILogger;


namespace modEformFuncApp
{
    class DependencyInjectionConfig
    {
        public DependencyInjectionConfig(string functionName)
        {
            DependencyInjection.Initialize(builder => { builder.RegisterType<DbLogger>().As<ILogger>(); },
                functionName);
        }
    }

    internal class DbLogger : ILogger
    {
        public DbLogger()
        {
        }

        public IDisposable BeginScope<TState>(TState state)
        {
            return null;
        }

        public bool IsEnabled(LogLevel logLevel)
        {
            return true;
        }

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state,
            Exception exception, Func<TState, Exception, string> formatter)
        {
            if (formatter == null)
            {
                throw new ArgumentNullException(nameof(formatter));
            }

            var message = formatter(state, exception);


            if (!string.IsNullOrEmpty(message) || exception != null)
            {
                switch (logLevel)
                {
                    case LogLevel.Critical:
                        Console.Error.WriteLine(message);
                        //_log.Fatal(message);
                        break;
                    case LogLevel.Debug:
                    case LogLevel.Trace:
                        Console.WriteLine(message);
                        //_log.Debug(message);
                        break;
                    case LogLevel.Error:
                        Console.Error.WriteLine(message);
                        //_log.Error(message);
                        break;
                    case LogLevel.Information:
                        Console.WriteLine(message);
                        //_log.Info(message);
                        break;
                    case LogLevel.Warning:
                        Console.Write(message);
                        //_log.Warn(message);
                        break;
                    default:
                        Console.Write(message);
                        //_log.Warn($"Unknown log level {logLevel}.\r\n{message}");
                        break;
                }
            }
        }
    }
}