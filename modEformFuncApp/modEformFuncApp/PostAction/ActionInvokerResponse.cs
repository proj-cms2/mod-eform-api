﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using modEformFuncApp.DBModels;

namespace modEformFuncApp.PostAction
{
    class ActionInvokerResponse
    {
        public List<ActionResponse> ActionResponses { get; set; }
        public EformModel Model { get; set; }
    }
}