﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using modEformFuncApp.Cms;
using modEformFuncApp.DBModels;
using Microsoft.Azure.Documents;
using Newtonsoft.Json;

namespace modEformFuncApp.PostAction
{
    class ActionInvoker
    {
        public static async Task<ActionInvokerResponse> InvokeActionAsync(EformModel model,
            IEnumerable<IPostAction> actions,
            string modelId, string tenant, string phase)
        {
            string cmsToken =
                await Login.getCMSLoginToken(tenant, "kioskuser", "c30b87A90b27@4c08a7eb$105c3f13EC72");


            var responses = new List<ActionResponse>();
            var tModel = JsonConvert.DeserializeObject<EformModel>(JsonConvert.SerializeObject(model));
            foreach (var a in actions)
            {
                if (a != null)
                {
                    if (a.options.executionLevel == phase)
                    {
                        var actionResp = await a.ExecuteAction(modelId, tenant, tModel, cmsToken);
                        actionResp.ActionType = a.type;
                        responses.Add(actionResp);
                        if (actionResp.Success)
                        {
                            tModel = actionResp.Model;
                        }
                    }
                }
                else
                {
                    responses.Add(new ActionResponse
                        {ActionType = "null", Success = false, Response = "action return a null object"});
                }
            }

            return new ActionInvokerResponse
            {
                ActionResponses = responses,
                Model = tModel
            };
        }
    }
}