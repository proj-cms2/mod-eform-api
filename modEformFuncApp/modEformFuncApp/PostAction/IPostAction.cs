﻿using System.Threading.Tasks;
using modEformFuncApp.DBModels;

namespace modEformFuncApp.PostAction
{
    internal interface IPostAction
    {
        string type { get; set; }
        dynamic options { get; set; }
        Task<ActionResponse> ExecuteAction(string modelId, string tenant, EformModel model, string cmsToken);
    }
}