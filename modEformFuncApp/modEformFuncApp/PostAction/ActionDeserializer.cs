﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using modEformFuncApp.PostAction.Type;

namespace modEformFuncApp.PostAction
{
    class ActionDeserializer
    {
        public static IPostAction Deserialize(dynamic a)
        {
            dynamic options = a.options;
            if (options == null)
            {
                options = new {executionLevel = "onApiCall"};
            }

            if (options.executionLevel == null)
            {
                options.executionLevel = "onApiCall";
            }

            if (a.type == "createPatient")
            {
                return new CreatePatientAction
                {
                    type = a.type,
                    options = options
                };
            }

            if (a.type == "uploadPdf")
            {
                return new UploadPdfAction
                {
                    type = a.type,
                    options = options
                };
            }

            if (a.type == "modifyModel")
            {
                options.code = a.options.code;
                return new ModifyModelAction
                {
                    type = a.type,
                    options = options
                };
            }

            return null;
        }
    }
}