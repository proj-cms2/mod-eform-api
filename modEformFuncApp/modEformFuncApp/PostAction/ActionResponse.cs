﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace modEformFuncApp.PostAction
{
    internal class ActionResponse
    {
        public string ActionType { get; set; }
        public bool Success { get; set; }
        public object Response { get; set; }
        public dynamic Model { get; set; }
    }
}