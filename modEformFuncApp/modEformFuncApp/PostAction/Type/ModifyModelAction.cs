﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Jint;
using Jint.Native;
using modEformFuncApp.DBModels;
using Newtonsoft.Json;

namespace modEformFuncApp.PostAction.Type
{
    class ModifyModelAction : IPostAction
    {
        public dynamic options { get; set; }
        public string type { get; set; }

        public async Task<ActionResponse> ExecuteAction(string modelId, string tenant, EformModel model, string cmsToken)
        {
            try
            {
                var engine = new Engine();
                engine.SetValue("modelStr", JsValue.FromObject(engine, model.eformModel));
                engine.Execute("var model = JSON.parse(modelStr)");

                string script = options.code;
                engine.Execute(script);
                engine.Execute("modelStr = JSON.stringify(model)");

                string mod = engine.GetValue("modelStr").AsString();

                model.eformModel = mod;

                return new ActionResponse {Success = true, Model = model};
            }
            catch (Exception e)
            {
                return new ActionResponse { Success = false, Response = e };
            }
        }
    }
}