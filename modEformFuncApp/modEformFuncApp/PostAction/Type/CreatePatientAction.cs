﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using modEformFuncApp.Cms;
using modEformFuncApp.DBModels;
using modEformFuncApp.Dtos;
using Newtonsoft.Json;

namespace modEformFuncApp.PostAction.Type
{
    class CreatePatientAction : IPostAction
    {
        public string type { get; set; }
        public dynamic options { get; set; }

        public async Task<ActionResponse> ExecuteAction(string modelId, string tenant, EformModel model,
            string cmsToken)
        {
            if (model.patientCreated == true)
            {
                return new ActionResponse {Success = false, Response = "Patient had already created"};
            }

            dynamic createPatientDto = JsonConvert.DeserializeObject(model.eformModel);

            HttpResponseMessage createPatientResponse =
                await CreatePatient.CmsCreatePatient(cmsToken, tenant, createPatientDto);


            var createPatientResponseText = await createPatientResponse.Content.ReadAsStringAsync();

            if (!createPatientResponse.IsSuccessStatusCode)
            {
                return new ActionResponse
                {
                    Success = false,
                    Response = createPatientResponseText
                };
            }

            try
            {
                dynamic returnObject = JsonConvert.DeserializeObject(createPatientResponseText);
                model.patientId = returnObject.result;
                model.patientCreated = true;

                return new ActionResponse
                {
                    Success = true,
                    Response = returnObject,
                    Model = model
                };
            }
            catch (Exception e)
            {
                return new ActionResponse
                {
                    Success = false,
                    Response = e
                };
            }
        }
    }
}