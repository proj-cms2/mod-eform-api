﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using modEformFuncApp.Cms;
using modEformFuncApp.DBModels;
using modEformFuncApp.Dtos;

namespace modEformFuncApp.PostAction.Type
{
    class UploadPdfAction : IPostAction
    {
        public dynamic options { get; set; }
        public string type { get; set; }

        public async Task<ActionResponse> ExecuteAction(string modelId, string tenant, EformModel model,
            string cmsToken)
        {
            long patientId = model.patientId;
            bool patientCreated = model.patientCreated ?? false;
            if (patientId == 0)
            {
                return new ActionResponse {Success = false, Response = "Patient not created"};
            }

            var stream = BlobStorage.GetPdf(model.id, tenant);

            if (stream == null)
            {
                return new ActionResponse {Success = false, Response = "No blob found in storage"};
            }

            try
            {
                var schema = await SchemaTable<EformSchema>.GetRawItemAsync(model.eformSchema, tenant);
                if (schema == null)
                {
                    return new ActionResponse {Success = false, Response = "Schema Not found"};
                }

                var uploadResp = await UploadAttachment.UploadToCms(tenant, "Bearer " + cmsToken,
                    patientId,
                    schema.eformName + ".pdf",
                    new StreamContent(stream));

                var uploadrespString = uploadResp.Content.ReadAsStringAsync();

                return new ActionResponse {Success = true, Response = uploadrespString, Model = model};
            }
            catch (Exception e)
            {
                return new ActionResponse {Success = false, Response = e};
            }
        }
    }
}