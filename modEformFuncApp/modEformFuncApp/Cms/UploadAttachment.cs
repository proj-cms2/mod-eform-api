﻿using Microsoft.Extensions.Logging;

namespace modEformFuncApp.Cms
{
    using Microsoft.Azure.WebJobs;
    using Microsoft.Azure.WebJobs.Extensions.Http;
    using Microsoft.Azure.WebJobs.Host;
    using modEformFuncApp.Dtos;
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Threading.Tasks;

    /// <summary>
    /// Defines the <see cref="UploadAttachment" />
    /// </summary>
    public class UploadAttachment
    {
        /// <summary>
        /// The Run
        /// </summary>
        /// <param name="req">The req<see cref="HttpRequestMessage"/></param>
        /// <param name="log">The log<see cref="ILogger"/></param>
        /// <param name="headers">The headers<see cref="IDictionary{string, string}"/></param>
        /// <returns>The <see cref="Task{HttpResponseMessage}"/></returns>
        [FunctionName("UploadAttachment")]
        public static async Task<HttpResponseMessage> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = null)]
            HttpRequestMessage req, ILogger log, IDictionary<string, string> headers)
        {
            headers.TryGetValue("Authorization", out var authHeader);
            if (string.IsNullOrEmpty(authHeader))
            {
                return req.CreateResponse(HttpStatusCode.Unauthorized);
            }

            log.LogInformation("C# HTTP trigger function processed a request.");


            try
            {
                //Store uploaded file to temp directory
                string tempDirectory = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
                Directory.CreateDirectory(tempDirectory);
                var provider = new MultipartFormDataStreamProvider(tempDirectory);
                var multipartProvider = await req.Content.ReadAsMultipartAsync(provider);
                var formData = multipartProvider.FormData;
                var fileData = multipartProvider.FileData;

                var dto = new UploadAttachmentDto
                {
                    tenant = formData["tenant"],
                    patientId = Int32.Parse(formData["patientId"]),
                    file = fileData[0]
                };

                if (string.IsNullOrEmpty(dto.tenant) || dto.file == null)
                {
                    return req.CreateResponse(HttpStatusCode.BadRequest,
                        "Please sure that you have upload a file and filled in the tenant field");
                }


                string Url = Utils.GetCMSDomain(dto.tenant) + "/DMS/upload";

                HttpClient httpClient = new HttpClient();
                httpClient.DefaultRequestHeaders.Add("Authorization", authHeader);

                MultipartFormDataContent form = new MultipartFormDataContent();

                form.Add(new StringContent(dto.patientId + ""), "patientId");

                var fileStream = new FileStream(dto.file.LocalFileName, FileMode.Open);
                var streamCtx = new StreamContent(fileStream);
                streamCtx.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                form.Add(streamCtx, "file", dto.file.Headers.ContentDisposition.FileName);

                HttpResponseMessage response = await httpClient.PostAsync(Url, form);

                return req.CreateResponse(response.StatusCode,
                    JsonConvert.DeserializeObject(await response.Content.ReadAsStringAsync()));
            }
            catch (Exception e)
            {
                log.LogInformation(e.Message.ToString());
                return req.CreateResponse(HttpStatusCode.InternalServerError, e.Message.ToString());
            }
        }


        public static async Task<HttpResponseMessage> UploadToCms(string tenant, string authHeader, long patientId,
            string fileName, HttpContent file)
        {
            string Url = Utils.GetCMSDomain(tenant) + "/DMS/upload";
            HttpClient httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Add("Authorization", authHeader);
            MultipartFormDataContent form = new MultipartFormDataContent();
            form.Add(new StringContent(patientId + ""), "patientId");
            file.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
            form.Add(file, "file", fileName);

            HttpResponseMessage response = await httpClient.PostAsync(Url, form);

            return response;
        }
    }
}