using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using modEformFuncApp.DBModels;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace modEformFuncApp.Cms
{
    public static class GetFormLink
    {
        /*
        public static async Task<HttpResponseMessage> GetRegistrationForm(HttpRequestMessage req, string host,
            string tenant,
            string locale)
        {
            bool localeFallback = false;
            if (tenant == null)
            {
                return req.CreateResponse(HttpStatusCode.BadRequest, "Tenant id is required");
            }

            if (locale == null)
            {
                locale = "en-us";
                localeFallback = true;
            }

            var form = (await SchemaTable<EformSchema>.GetItemsAsync(r =>
                r.eformName == "Registration Form" && r.tenant == "*" && r.isDeleted != true)).FirstOrDefault();

            if (form == null)
            {
                return req.CreateResponse(HttpStatusCode.InternalServerError, "Root registration form not found");
            }

            string rootFormId = form.rootFormId;

            EformSchema tenantForm = await SchemaTable<EformSchema>.GetItemAsync(rootFormId, tenant);

            if (locale != "en-us")
            {
                if (tenantForm != null)
                {
                    if (tenantForm.eformLocale == null || !JsonConvert
                            .DeserializeObject<Dictionary<string, dynamic>>(tenantForm.eformLocale).ContainsKey(locale))
                    {
                        locale = "en-us";
                        localeFallback = true;
                    }
                }
                else
                {
                    if (form.eformLocale == null || !JsonConvert
                            .DeserializeObject<Dictionary<string, dynamic>>(form.eformLocale).ContainsKey(locale))
                    {
                        locale = "en-us";
                        localeFallback = true;
                    }
                }
            }

            return req.CreateResponse(HttpStatusCode.OK, new
            {
                formLink = $"{host}/eFormView?form={rootFormId}&tenant={tenant}&locale={locale}",
                localeFallback
            });
        }

            */

        class FormResult
        {
            public string FormLink { get; set; }
            public string FormName { get; set; }
            public bool FormLocaleFallback { get; set; }
        }


        public static async Task<HttpResponseMessage> GetAllForm(HttpRequestMessage req, string host,
            string tenant,
            string locale)
        {
            bool localeFallback = false;
            if (tenant == null)
            {
                return req.CreateResponse(HttpStatusCode.BadRequest, "Tenant id is required");
            }


            if (locale == null)
            {
                locale = "en-us";
                localeFallback = true;
            }


            var list = await SchemaTable<EformSchema>.GetItemsAsync(s =>
                (s.tenant == tenant || s.tenant == "*") && s.isDeleted != true);
            var formList = list.Select(s => s.rootFormId).Distinct();
            var distictList = formList.Select(s =>
            {
                return list.FirstOrDefault(s2 => s2.rootFormId == s && s2.tenant == tenant) ??
                       list.FirstOrDefault(s2 => s2.rootFormId == s && s2.tenant == "*");
            }).Where(s => s != null && s.isPrivate != true).Select(form =>
            {
                var formLocaleFallback = localeFallback;
                var formLocale = locale;


                if (!formLocaleFallback)
                {
                    if (form.eformLocale == null || !JsonConvert
                            .DeserializeObject<Dictionary<string, dynamic>>(form.eformLocale).Keys
                            .Select(l => l.ToLower()).Contains(locale))
                    {
                        formLocale = form.defaultLanguage ?? "en-us";
                        formLocaleFallback = true;
                    }
                }
                else
                {
                    if (form.eformLocale == null || !JsonConvert
                            .DeserializeObject<Dictionary<string, dynamic>>(form.eformLocale).Keys
                            .Select(l => l.ToLower()).Contains(locale))
                    {
                        formLocale = form.defaultLanguage;
                    }
                }

                return new FormResult
                {
                    FormLink = $"{host}/eFormView?form={form.rootFormId}&tenant={tenant}&locale={formLocale}",
                    FormName = form.eformName,
                    FormLocaleFallback = formLocaleFallback,
                };
            });


            return req.CreateResponse(HttpStatusCode.OK, distictList);
        }


        [FunctionName("GetFormLink")]
        public static async Task<HttpResponseMessage> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = null)]
            HttpRequestMessage req, ILogger log)
        {
            var host = Environment.GetEnvironmentVariable("EformHost");
            // Get request body
            /*
            dynamic data = await req.Content.ReadAsAsync<object>();
            string tenant = data?.tenant;
            string locale = data?.locale;
            string formLost = data?.formList;
            */
            string tenant = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, "tenant", true) == 0)
                .Value;

            string locale = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, "locale", true) == 0)
                .Value;
            return await GetAllForm(req, host, tenant, locale);
        }
    }
}