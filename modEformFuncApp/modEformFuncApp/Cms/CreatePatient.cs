﻿using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using modEformFuncApp.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Results;
using System.Web.Script.Serialization;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace modEformFuncApp.Cms
{
    public static class CreatePatient
    {
        [FunctionName("CreatePatient")]
        public static async Task<HttpResponseMessage> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = null)]
            HttpRequestMessage req, ILogger log, IDictionary<string, string> headers)
        {
            // content type must is "application/json"
            if (!req.Content.Headers.ContentType.ToString().Contains("json"))
                return req.CreateResponse(HttpStatusCode.BadRequest,
                    "Invalid ContentType");

            dynamic createPatientInputDto = await req.Content.ReadAsAsync<dynamic>();

            // parse query parameter
            string tenant = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, "tenant", true) == 0)
                .Value;


            headers.TryGetValue("Authorization", out var authHeader);
            authHeader = authHeader?.Replace("Bearer ", "");

            HttpResponseMessage response = await CmsCreatePatient(authHeader, tenant, createPatientInputDto);


            var responseStr = await response.Content.ReadAsStringAsync();

            return req.CreateResponse(response.StatusCode, responseStr);
        }

        public static async Task<HttpResponseMessage> CmsCreatePatient(string token, string tenant,
            dynamic createPatientObj)
        {
            string Url = Utils.GetCMSDomain(tenant) + "/api/services/app/Patient/CreatePatient";


            var content = new StringContent(JsonConvert.SerializeObject(createPatientObj), Encoding.UTF8,
                "application/json");


            if (string.IsNullOrEmpty(token))
            {
                return new HttpResponseMessage(HttpStatusCode.Unauthorized);
            }

            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var response = await client.PostAsync(Url, content);

            return response;
        }
    }
}