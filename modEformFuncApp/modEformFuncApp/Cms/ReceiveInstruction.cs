using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http.Results;
using Jose;
using modEformFuncApp.DBModels;
using modEformFuncApp.Dtos;
using modEformFuncApp.PostAction;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace modEformFuncApp.Cms
{
    public static class ReceiveInstruction
    {
        [FunctionName("ReceiveInstruction")]
        public static async Task<HttpResponseMessage> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = null)]
            HttpRequestMessage req, ILogger log, IDictionary<string, string> headers)
        {
            // content type must is "application/json"
            //if (!req.Content.Headers.ContentType.ToString().Contains("json"))
            //    return req.CreateResponse(HttpStatusCode.BadRequest,
            //        "Invalid ContentType");

            //headers.TryGetValue("Authorization", out var authHeader);
            //authHeader = authHeader?.Replace("Bearer ", "");


            dynamic body = await req.Content.ReadAsAsync<dynamic>();
            string id = body.id;
            string tenant = body.tenant;
            string patientId = body.patientId;

            var model = await ModelTable<EformModel>.GetItemAsync(id, tenant);

            if (model == null)
            {
                return req.CreateResponse(HttpStatusCode.BadRequest, "model not found");
            }

            if (patientId != null)
            {
                try
                {
                    model.patientId = long.Parse(patientId);
                }
                catch (Exception)
                {
                    return req.CreateResponse(HttpStatusCode.BadRequest, "Invalid patient id");
                }

                await ModelTable<EformModel>.UpdateItemAsync(id, model);
            }


            var schema = await SchemaTable<EformSchema>.GetRawItemAsync(model.eformSchema, tenant);
            if (schema == null)
            {
                return req.CreateResponse(HttpStatusCode.BadRequest, "schema not found");
            }


            if (schema.postAction == null)
            {
                return req.CreateResponse(HttpStatusCode.OK,
                    new {success = true, id = model.id, response = new {message = "No action performed"}});
            }


            var actionsJson = JsonConvert.DeserializeObject<List<dynamic>>(schema.postAction);
            var actions = actionsJson.Select<dynamic, IPostAction>(a => ActionDeserializer.Deserialize(a));

            var actionInvokerResponse =
                await ActionInvoker.InvokeActionAsync(model, actions, model.id, tenant, "onApiCall");

            await ModelTable<EformModel>.UpdateItemAsync(model.id, actionInvokerResponse.Model);


            return req.CreateResponse(HttpStatusCode.OK,
                new {success = true, id = model.id, response = actionInvokerResponse.ActionResponses});
        }
    }
}