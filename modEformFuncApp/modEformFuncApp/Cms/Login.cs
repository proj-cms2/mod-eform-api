﻿using modEformFuncApp.ApiEformModel;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace modEformFuncApp.Cms
{
    using Microsoft.Azure.WebJobs;
    using Microsoft.Azure.WebJobs.Extensions.Http;
    using Microsoft.Azure.WebJobs.Host;
    using modEformFuncApp.Dtos;
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Net.Http;
    using System.Net.Http.Formatting;
    using System.Threading.Tasks;

    /// <summary>
    /// Defines the <see cref="Login" />
    /// </summary>
    public static class Login
    {
        /// <summary>
        /// The Run
        /// </summary>
        /// <param name="req">The req<see cref="HttpRequestMessage"/></param>
        /// <param name="log">The log<see cref="ILogger"/></param>
        /// <returns>The <see cref="Task{HttpResponseMessage}"/></returns>
        [FunctionName("LoginFromCMS")]
        public static async Task<HttpResponseMessage> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = null)]
            HttpRequestMessage req, ILogger log)
        {
            // content type must is "application/json"
            if (!req.Content.Headers.ContentType.ToString().Contains("json"))
                return req.CreateResponse(HttpStatusCode.BadRequest,
                    "Invalid ContentType");

            LoginInputDto loginInputDto = await req.Content.ReadAsAsync<LoginInputDto>();

            // valid parameters 
            if (string.IsNullOrEmpty(loginInputDto.tenant) || string.IsNullOrEmpty(loginInputDto.pass) ||
                string.IsNullOrEmpty(loginInputDto.user))
                return req.CreateResponse(HttpStatusCode.BadRequest,
                    "Invalid parameters");

            // call external api to get login token (CMS)
            var responseStr = await loginFromCMSAsync(loginInputDto.tenant, loginInputDto.user, loginInputDto.pass);

            // output as JSON format


            return req.CreateResponse(HttpStatusCode.OK, responseStr, JsonMediaTypeFormatter.DefaultMediaType);
        }

        /// <summary>
        /// The loginFromCMSAsync
        /// </summary>
        /// <param name="tenant">The tenant<see cref="string"/></param>
        /// <param name="user">The user<see cref="string"/></param>
        /// <param name="pass">The pass<see cref="string"/></param>
        /// <returns>The <see cref="Task{dynamic}"/></returns>
        public static async Task<string> loginFromCMSAsync(string tenant, string user, string pass)
        {
            //string Url = "https://idm-clinic1-cms.azurewebsites.net/connect/token";
            string Url = Utils.GetCMSTokenUrl(tenant);

            var values = Utils.GetCMSTokenValues(tenant, user, pass);

            var content = new FormUrlEncodedContent(values);

            HttpClient client = new HttpClient();
            var response = await client.PostAsync(Url, content);
            return await response.Content.ReadAsStringAsync();

            //LoginOutputDto loginOutputDto = await response.Content.ReadAsAsync<LoginOutputDto>();
            //return loginOutputDto;
        }

        public static async Task<string> getCMSLoginToken(string tenant, string user, string pass)
        {
            var loginResponse =
                await Login.loginFromCMSAsync(tenant,user, pass);
            var lrDto = JsonConvert.DeserializeObject<LoginOutputDto>(loginResponse);
            return lrDto.access_token;
        }
    }
}