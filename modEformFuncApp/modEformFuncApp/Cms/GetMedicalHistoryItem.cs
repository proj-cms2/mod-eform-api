using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using modEformFuncApp.DBModels;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Logging;

namespace modEformFuncApp.Cms
{
    public static class GetMedicalHistoryItem
    {
        [FunctionName("GetMedicalHistoryItem")]
        public static async Task<HttpResponseMessage> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)]
            HttpRequestMessage req, ILogger log)
        {
            var items = await MedicalHistoryItemTable<EformMedicalHistoryItem>.GetItemsAsync();

            // parse query parameter
            var defaultLocale = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, "defaultLocale", StringComparison.OrdinalIgnoreCase) == 0)
                .Value;

            var locale = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, "locale", StringComparison.OrdinalIgnoreCase) == 0)
                .Value;

            if (string.IsNullOrEmpty(defaultLocale))
            {
                defaultLocale = "en-US";
            }

            if (string.IsNullOrEmpty(locale))
            {
                locale = defaultLocale;
            }

            Dictionary<string, string> data = new Dictionary<string, string>();

            locale = locale.ToLower();

            foreach (var i in items)
            {
                Dictionary<string, string> loc = i.locale;
                var locDict = loc.Select(pair =>
                    new KeyValuePair<string, string>(pair.Key.ToLower(), pair.Value)
                ).ToDictionary(a => a.Key, a => a.Value);

                locDict.TryGetValue(locale, out var v1);
                locDict.TryGetValue(locale, out var v2);
                data.Add(i.key, v1 ?? v2 ?? i.key);
            }


            return req.CreateResponse(HttpStatusCode.OK, data);
        }
    }
}