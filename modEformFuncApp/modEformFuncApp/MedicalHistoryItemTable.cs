﻿using modEformFuncApp.DBModels;
using modEformFuncApp.Dtos;

namespace modEformFuncApp
{
    using Microsoft.Azure.Documents;
    using Microsoft.Azure.Documents.Client;
    using Microsoft.Azure.Documents.Linq;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;

    /// <summary>
    /// Defines the <see cref="SchemaTable{T}" />
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class MedicalHistoryItemTable<T> where T : class
    {
        /// <summary>
        /// Defines the client
        /// </summary>
        private static DocumentClient client =
            CosmosDB.Connect(Environment.GetEnvironmentVariable("CosmosDbMedicalHistoryItemCollection"));

        /// <summary>
        /// Defines the DatabaseId
        /// </summary>
        private static string DatabaseId = Environment.GetEnvironmentVariable("CosmosDbDatabase");

        /// <summary>
        /// Defines the CollectionId
        /// </summary>
        private static string CollectionId = Environment.GetEnvironmentVariable("CosmosDbMedicalHistoryItemCollection");

        public static async Task<IEnumerable<T>> GetItemsAsync()
        {
            IDocumentQuery<T> query = client.CreateDocumentQuery<T>(
                    UriFactory.CreateDocumentCollectionUri(DatabaseId, CollectionId),
                    new FeedOptions {MaxItemCount = -1, EnableCrossPartitionQuery = true})
                .AsDocumentQuery();

            List<T> results = new List<T>();
            while (query.HasMoreResults)
            {
                results.AddRange(await query.ExecuteNextAsync<T>());
            }

            return results;
        }

    
    }
}