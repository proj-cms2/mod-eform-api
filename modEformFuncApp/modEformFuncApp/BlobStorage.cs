﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Logging;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace modEformFuncApp
{
    class BlobStorage
    {
        private static readonly string ConnectionString =
            Environment.GetEnvironmentVariable("BlobStorageConnectionString");

        private static readonly string ContainerName = Environment.GetEnvironmentVariable("BlobStorageContainer");

        public static async Task UploadPdf(string id, string tenant, string b64str)
        {
            var account = CloudStorageAccount.Parse(ConnectionString);
            var blobClient = account.CreateCloudBlobClient();
            var container = blobClient.GetContainerReference(ContainerName);
            var dir = container.GetDirectoryReference(tenant);
            var blob = dir.GetAppendBlobReference(id + ".pdf");
            var file = Convert.FromBase64String(b64str);
            await blob.UploadFromByteArrayAsync(file, 0, file.Length);
        }

        public static Stream GetPdf(string id, string tenant)
        {
            var account = CloudStorageAccount.Parse(ConnectionString);
            var blobClient = account.CreateCloudBlobClient();
            var container = blobClient.GetContainerReference(ContainerName);
            var dir = container.GetDirectoryReference(tenant);
            var blob = dir.GetBlobReference(id + ".pdf");
            if (blob.Exists())
            {
                return blob.OpenRead();
            }

            return null;
        }

        public static Dictionary<string, string> GetTenantPdf(string tenant)
        {
            var account = CloudStorageAccount.Parse(ConnectionString);
            var blobClient = account.CreateCloudBlobClient();
            var container = blobClient.GetContainerReference(ContainerName);
            var dir = container.GetDirectoryReference(tenant);
            var sas = container.GetSharedAccessSignature(new SharedAccessBlobPolicy
            {
                Permissions = SharedAccessBlobPermissions.Read,
                SharedAccessExpiryTime = DateTimeOffset.UtcNow.Add(TimeSpan.FromMinutes(15)),
            });

            Dictionary<string, string> pdfs = new Dictionary<string, string>();
            foreach (var blob in dir.ListBlobs())
            {
                pdfs.Add(Path.GetFileNameWithoutExtension(blob.Uri.LocalPath),
                    blob.Uri + sas);
            }

            return pdfs;
        }
    }
}