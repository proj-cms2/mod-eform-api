﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace modEformFuncApp.DBModels
{
    public class EformModel
    {
        [JsonProperty(PropertyName = "id")] public string id { get; set; }

        [JsonProperty(PropertyName = "tenant")]
        public string tenant { get; set; }

        [JsonProperty(PropertyName = "patientId")]
        public long patientId { get; set; }

        [JsonProperty(PropertyName = "patientCreated")]
        public bool? patientCreated { get; set; }

        [JsonProperty(PropertyName = "creationTime")]
        public DateTime creationTime { get; set; }

        [JsonProperty(PropertyName = "creatorUserId")]
        public string creatorUserId { get; set; }

        [JsonProperty(PropertyName = "lastModificationTime")]
        public DateTime? lastModificationTime { get; set; }

        [JsonProperty(PropertyName = "lastModifierUserId")]
        public string lastModifierUserId { get; set; }

        [JsonProperty(PropertyName = "deletionTime")]
        public DateTime? deletionTime { get; set; }

        [JsonProperty(PropertyName = "deleterUserId")]
        public string deleterUserId { get; set; }

        [JsonProperty(PropertyName = "isDeleted")]
        public bool? isDeleted { get; set; }

        [JsonProperty(PropertyName = "eformModel")]
        public string eformModel { get; set; }

        [JsonProperty(PropertyName = "eformSchema")]
        public string eformSchema { get; set; }

        [JsonProperty(PropertyName = "formTitle")]
        public string formTitle { get; set; }

        [JsonProperty(PropertyName = "formSubtitle")]
        public string formSubtitle { get; set; }

        [JsonProperty(PropertyName = "submitLocale")]
        public string submitLocale { get; set; }

        [JsonProperty(PropertyName = "pdfDoc")]
        public string pdfDoc { get; set; }

        [JsonProperty(PropertyName = "postActionHistory")]
        public dynamic postActionHistory { get; set; }


    }
}