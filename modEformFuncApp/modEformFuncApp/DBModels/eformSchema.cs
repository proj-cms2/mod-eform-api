﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace modEformFuncApp.DBModels
{
    public class EformSchema
    {
        [JsonProperty(PropertyName = "id")] public string id { get; set; }

        [JsonProperty(PropertyName = "tenant")]
        public string tenant { get; set; }

        [JsonProperty(PropertyName = "creationTime")]
        public DateTime creationTime { get; set; }

        [JsonProperty(PropertyName = "creatorUserId")]
        public string creatorUserId { get; set; }

        [JsonProperty(PropertyName = "lastModificationTime")]
        public DateTime? lastModificationTime { get; set; }

        [JsonProperty(PropertyName = "lastModifierUserId")]
        public string lastModifierUserId { get; set; }

        [JsonProperty(PropertyName = "deletionTime")]
        public DateTime? deletionTime { get; set; }

        [JsonProperty(PropertyName = "deleterUserId")]
        public string deleterUserId { get; set; }

        [JsonProperty(PropertyName = "isDeleted")]
        public bool? isDeleted { get; set; }

        [JsonProperty(PropertyName = "eformName")]
        public string eformName { get; set; }

        [JsonProperty(PropertyName = "eformSchema")]
        public string eformSchema { get; set; }

        [JsonProperty(PropertyName = "eformDefaultModel")]
        public string eformDefaultModel { get; set; }

        [JsonProperty(PropertyName = "zh_HK_eformSchema")]
        public string zh_HK_eformSchema { get; set; }

        [JsonProperty(PropertyName = "zh_HK_eformDefaultModel")]
        public string zh_HK_eformDefaultModel { get; set; }

        [JsonProperty(PropertyName = "version")]
        public int version { get; set; }

        [JsonProperty(PropertyName = "rootFormVersion")]
        public int? rootFormVersion { get; set; }

        [JsonProperty(PropertyName = "rootFormId")]
        public string rootFormId { get; set; }

        [JsonProperty(PropertyName = "defaultLanguage")]
        public string defaultLanguage { get; set; }

        [JsonProperty(PropertyName = "eformLocale")]
        public string eformLocale { get; set; }


        [JsonProperty(PropertyName = "availableLanguage")]
        public ICollection<string> availableLanguage { get; set; }

        [JsonProperty(PropertyName = "formLocale")]
        public string formLocale { get; set; }

        [JsonProperty(PropertyName = "languageNotAvailable")]
        public bool? languageNotAvailable { get; set; }

        [JsonProperty(PropertyName = "postAction")]
        public string postAction { get; set; }


        [JsonProperty(PropertyName = "tenantFormSourceVersion")]
        public int? tenantFormSourceVersion { get; set; }


        [JsonProperty(PropertyName = "rootRemark")]
        public string rootRemark { get; set; }


        [JsonProperty(PropertyName = "isPrivate")]
        public bool? isPrivate { get; set; }
    }
}