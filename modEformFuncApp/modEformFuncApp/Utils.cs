﻿using System;
using Jose;
using modEformFuncApp.DBModels;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Azure.WebJobs.Logging;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace modEformFuncApp
{
    using System.Collections.Generic;
    using System.IdentityModel.Tokens.Jwt;
    using System.Linq;

    /// <summary>
    /// Defines the <see cref="Utils" />
    /// </summary>
    public static class Utils
    {
        /// <summary>
        /// The DecodeToken
        /// </summary>
        /// <param name="bearerToken">The bearerToken<see cref="string"/></param>
        /// <returns>The <see cref="JwtSecurityToken"/></returns>
        private static JwtSecurityToken DecodeToken(string bearerToken)
        {
            var jwtEncodedString = bearerToken.Substring(7);
            return new JwtSecurityToken(jwtEncodedString: jwtEncodedString);
        }

        /// <summary>
        /// The GetClaimsFromToken
        /// </summary>
        /// <param name="token">The token<see cref="string"/></param>
        /// <param name="key">The key<see cref="string"/></param>
        /// <returns>The <see cref="string"/></returns>
        public static string GetClaimsFromToken(string token, string key)
        {
            return DecodeToken(token).Claims.First(c => c.Type == key).Value;
        }

        private const string JwtSecret =
            "09j9034)($JT#$TPJ()UTJ#J($INGT)($J%U)($#T98w4ijgt4039je0j9tT(MJ)#()J94iwjtmgoe94tjg0(JTM#$(JGTM09iw4jmtg0o94tg)(MIOT$#J)(YMo)JO$Y309jmogteM)(J$#MTOw03jirmo4efgjm4309jgt40)(J#$MITG$)#(INTI)W#RF)W(JIMRO#$I(JRT#$OMJ)TI$JMT$#)J(MTOIG$#I)GNO$#)JIMGW$EGN)W$J(NIT";

        public static string CreateSubmitToken(string uuid, string tenant, long validTime)
        {
            var jwt = JWT.Encode(new
            {
                uuid,
                tenant,
                expire = DateTime.Now.AddMilliseconds(validTime)
            }, JwtSecret, JweAlgorithm.PBES2_HS256_A128KW, JweEncryption.A128CBC_HS256);
            return jwt;
        }

        public static dynamic DecodeSubmitToken(string token)
        {
            var jwt = new JwtSecurityToken(jwtEncodedString: JWT.Decode(token, JwtSecret,
                JweAlgorithm.PBES2_HS256_A128KW, JweEncryption.A128CBC_HS256));
            return jwt.Payload;
        }

        public static string GetCMSDomain(string tenant)
        {
            return "https://" + tenant + ".clinic1.one";
        }

        public static string GetCMSTokenUrl(string tenant)
        {
            //return "https://" + tenant + ".clinic1.one/connect/token"
            return "https://idm-clinic1-cms.azurewebsites.net/connect/token";
        }

        public static Dictionary<string, string> GetCMSTokenValues(string tenant, string user, string pass)
        {
            //return new Dictionary<string, string>
            //{
            //   { "client_id", "client" },
            //   { "client_secret", "secret" },
            //   { "grant_type", "module" },
            //   { "username", user },
            //   { "password", pass },
            //   { "tenancyname", tenant }
            //}; 

            return new Dictionary<string, string>
            {
                {"scope", "internalApi"},
                {"client_id", "internalClient"},
                {"client_secret", "internalSecret"},
                {"grant_type", "internal"},
                {"username", user},
                {"password", pass},
                {"tenancyname", tenant}
            };
        }

        public static ILogger log = null;

        public static EformSchema ReplaceLanguage(this EformSchema schemaItem, string convertLang)
        {
            if (schemaItem.eformLocale == null)
            {
                return schemaItem;
            }

            var schema = JsonConvert.DeserializeObject<dynamic>(schemaItem.eformSchema);
            string efLocale = schemaItem.eformLocale;
            string defLang = schemaItem.defaultLanguage;
            convertLang = convertLang ?? defLang;
            var langCollection =
                JsonConvert.DeserializeObject<Dictionary<string, Dictionary<string, string>>>(efLocale);
            schemaItem.availableLanguage = langCollection.Keys;
            schemaItem.languageNotAvailable = false;
            Dictionary<string, string> lang = null;

            lang = langCollection.FirstOrDefault(kvp => kvp.Key.Equals(convertLang, StringComparison.OrdinalIgnoreCase))
                .Value;
            if (lang == null && convertLang != defLang)
            {
                convertLang = defLang;
                schemaItem.languageNotAvailable = true;
            }

            if (lang == null)
            {
                lang = langCollection
                    .FirstOrDefault(kvp => kvp.Key.Equals(convertLang, StringComparison.OrdinalIgnoreCase)).Value;
            }

            if (lang == null)
            {
                return schemaItem;
            }

            schemaItem.formLocale = convertLang;

            if (schema.title != null)
            {
                string title = schema.title;
                lang.TryGetValue(title, out title);
                schema.title = title ?? schema.title;
            }

            if (schema.subtitle != null)
            {
                string subtitle = schema.subtitle;
                lang.TryGetValue(subtitle, out subtitle);
                schema.subtitle = subtitle ?? schema.subtitle;
            }

            if (schema.groups != null)
            {
                foreach (dynamic g in schema.groups)
                {
                    if (g.legend != null)
                    {
                        string legend = g.legend;
                        lang.TryGetValue(legend, out legend);
                        g.legend = legend ?? g.legend;
                    }

                    if (g.fields != null)
                    {
                        foreach (dynamic f in g.fields)
                        {
                            if (f.label != null)
                            {
                                string label = f.label;
                                lang.TryGetValue(label, out label);
                                f.label = label ?? f.label;
                            }

                            if (f.title != null)
                            {
                                string title = f.title;
                                lang.TryGetValue(title, out title);
                                f.title = title ?? f.title;
                            }

                            if (f.placeholder != null)
                            {
                                string placeholder = f.placeholder;
                                lang.TryGetValue(placeholder, out placeholder);
                                f.placeholder = placeholder ?? f.placeholder;
                            }

                            if (f.choices != null)
                            {
                                foreach (var c in f.choices)
                                {
                                    if (c.label != null)
                                    {
                                        string label = c.label;
                                        lang.TryGetValue(label, out label);
                                        c.label = label ?? c.label;
                                    }
                                }
                            }

                            if (f.values != null && f.itemText != null)
                            {
                                string itemText = f.itemText;
                                foreach (var v in f.values)
                                {
                                    if (v[itemText] != null)
                                    {
                                        string iTxt = v[itemText];
                                        lang.TryGetValue(iTxt, out iTxt);
                                        v[itemText] = iTxt ?? v[itemText];
                                    }
                                }
                            }
                        }
                    }
                }
            }

            schemaItem.eformSchema = JsonConvert.SerializeObject(schema);
            return schemaItem;
        }
    }
}